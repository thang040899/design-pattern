﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Product
{
    public interface Animal
    {
        string Speak();
    }
}
