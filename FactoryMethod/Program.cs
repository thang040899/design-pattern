﻿using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input type animal: ");
            String type = Console.ReadLine();
            var Voice=AnimalFactory.GetAnimal(type).Speak();
            Console.WriteLine(Voice);
            Console.ReadKey();
        }
    }
}
