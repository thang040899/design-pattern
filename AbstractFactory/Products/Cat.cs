﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AbstractFactory.Products
{
    public class Cat: Animal
    {
        public string Speak()
        {
            return "Mèo méo meo mèo meo!";
        }
    }
}
