﻿using System;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            //Tao factory
            AnimalFactory animalFactory = AnimalFactory.CreateAnimalFactory("Air");
            Console.WriteLine("AnimalFactory: " + animalFactory.GetType().Name);

            Animal animal = animalFactory.GetAnimal("Bird");
            Console.WriteLine("Animal said: "+animal.Speak());

            animal = animalFactory.GetAnimal("Butterfly");
            Console.WriteLine("Animal said: " + animal.Speak());

            Console.WriteLine("------------CREATE NEW FACTORY-----------");
            animalFactory = AnimalFactory.CreateAnimalFactory("Land");
            Console.WriteLine("AnimalFactory: " + animalFactory.GetType().Name);

            animal = animalFactory.GetAnimal("Cat");
            Console.WriteLine("Animal said: " + animal.Speak());


        }
    }
}
